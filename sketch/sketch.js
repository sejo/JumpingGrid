// Arrow by José Campos from the Noun Project
//
//
// Assets to load
var font;
var font_bold;
var img_arrow;

// Geomety
var cardWidth;
var cardHeight;
var cellsize;
var NROWS = 6;
var NCOLS = 4;
var margin;
var leftMargin;

var texto ;

var pointers = [];
var isCharTaken = [];
var lettersPos = [];

function preload(){
	img_arrow = loadImage("data/arrow_white_100.png");
	font = loadFont("data/Tulia_regular.otf");
	font_bold = loadFont("data/Tulia_bold.otf");
}

function setup() {
	createCanvas(800,700);
	cardWidth = width/2;
	cardHeight = height;

	textFont(font);

	texto = "sejo escenaconsejo.org !";

	for(var i=0; i<texto.length; i++){
		isCharTaken.push(false);
		pointers.push(i);
		lettersPos.push(0);
	}

	isCharTaken[0] = true;
	var trueCount = 1;

	var newPointer;
	var p = 0;
	var count = 0;
	for(var count=0; count<pointers.length-1; count++){
		// Get a fresh new pointer
		do{
			newPointer = int(random(texto.length));
		}while(isCharTaken[newPointer]);

		pointers[p] = newPointer;
		isCharTaken[newPointer] = true;
		p = newPointer;
	}

	pointers[p] = 0;

	// Print the pointers
	for(var i=0; i<pointers.length; i++){
		println(i+" "+pointers[i]);

	}


	// Make the opposite list
	var i = 0;
	var pos = 0;
	for(var i=0;i<pointers.length;i++){
		lettersPos[pos] = i;
		pos = pointers[pos];
	}

	for(var i=0; i<pointers.length; i++){
		println(i+" "+lettersPos[i]);

	}


	cellsize = 0.8*cardWidth/4;
	margin = 0.1*cardWidth/4;

  
}

function draw() {
	background(255);
	noFill();
	rect(0,0,cardWidth-1,cardHeight-1);
	rect(cardWidth,0,cardWidth-1,cardHeight-1);




	var tileNumber = 0;
	var x = margin;
	var y = 0;

	var totalW = NCOLS*cellsize + (NCOLS-1)*margin;

	leftMargin = (cardWidth-totalW)/2;

	translate(leftMargin,leftMargin);

	var delta = 5;
	var jump;
	var letterPointer =  0;
	for(var i=0; i<NROWS; i++){
		x = 0;
		for(var j=0; j<NCOLS; j++){
			jump = (tileNumber + delta)%(NROWS*NCOLS);
			letterPointer = int(tileNumber/delta) + delta*(tileNumber%delta);
			jump = pointers[tileNumber];
			letterPointer = lettersPos[tileNumber];
				
			drawSquare(x,y,cellsize,texto.charAt(int(letterPointer)),tileNumber,jump);

			tileNumber++;

			x+=cellsize+margin;

		}
		y += cellsize+margin;
	}

	translate(cardWidth-leftMargin,0);

	y = 0;
	tileNumber = 0;
	for(var i=0; i<NROWS; i++){
		x = cardWidth-leftMargin-cellsize;
		for(var j=0; j<NCOLS; j++){
				
			drawSquare2(x,y,cellsize,tileNumber);
			

			if(pointers[tileNumber]!=0){
			drawLine(tileNumber,pointers[tileNumber]);
			}


			tileNumber++;

			x-= (cellsize+margin);

		}
		y += cellsize+margin;
	}

	//drawLine(0,6);

  
}

function mousePressed(){
	saveCanvas("Saved","png");
}

// x,y, size, letter to display, 
function drawSquare(x,y,s,letter,number, nextnumber){
	var radiusP = 0.2; // Radius percentage
	var strokeWeightP = 0.02;
	var leftMarginNumberP = 0.10;
	var topMarginNumberP = 0.25;

	var numberSize = 0.3;

	var bottomAreaHeightP = 0.25;
	var bottomAreaYP = 1-bottomAreaHeightP;

	stroke(0);

	push();
	translate(x,y);
	scale(s);

	fill(255);
	if(number==0){
	strokeWeight(strokeWeightP*5);
	}
	else{
	strokeWeight(strokeWeightP);
	}
	rect(0,0,1,1,radiusP);

	fill(0);
	rect(0,bottomAreaYP,1,bottomAreaHeightP, 0,0, radiusP, radiusP);

	var imgYP = 1-bottomAreaHeightP*0.9;
	var imghP = bottomAreaHeightP*0.7;
	var imgwP = imghP*img_arrow.width/img_arrow.height;
	var imgXP = 0.2;



	// Texts
	// Number
	textFont(font_bold);
	fill(0);
	noStroke();
	var fontSize_numberP = 0.2;
	textSize(fontSize_numberP);
	text(number,leftMarginNumberP,topMarginNumberP);

	// Letter
	if(number==0){
		textFont(font_bold);
	}
	else{
		textFont(font);
	}
	var fontSize_letterP = 0.65;
	textSize(fontSize_letterP);
	var textw = textWidth(letter);

	text(letter,(1-textw)/2,fontSize_letterP*0.85);


	// Next number
	// Arrow
	if(nextnumber!=0){
	image(img_arrow, imgXP, imgYP, imgwP, imghP);
	fill(255);
	textFont(font_bold);
	var fontSize_nextnumberP = 0.22;
	textSize(fontSize_nextnumberP);
	text(nextnumber,imgXP + imgwP+0.01,bottomAreaYP+fontSize_nextnumberP);
	}

	pop();

}

// Sorry for the function duplicate and bad coding practice...
function drawSquare2(x,y,s,number){
	var radiusP = 0.2; // Radius percentage
	var strokeWeightP = 0.01;
	var leftMarginNumberP = 0.10;
	var topMarginNumberP = 0.25;

	var numberSize = 0.3;

	var bottomAreaHeightP = 0.25;
	var bottomAreaYP = 1-bottomAreaHeightP;

	stroke(0);

	push();
	translate(x,y);
	scale(s);

	noFill();
	if(number==0){
	strokeWeight(strokeWeightP*5);
	}
	else{
	strokeWeight(strokeWeightP);
	}

//	rect(0,0,1,1,radiusP);

	var innerRectS = 0.1;
	var innerPos = (1-innerRectS)/2;
	fill(0);
	rect(innerPos,innerPos,innerRectS,innerRectS,radiusP);
	pop();

}

// Draw a line from the center of a tile to the other 
function drawLine(begin,end){
	var rbegin = int(begin/NCOLS);
	var cbegin = NCOLS-1-int(begin%NCOLS);

	var xbegin = cbegin*(cellsize+margin) + 0.5*cellsize;
	var ybegin = rbegin*(cellsize+margin) + 0.5*cellsize;
	

	var rend = int(end/NCOLS);
	var cend = NCOLS-1-int(end%NCOLS);
	var xend = cend*(cellsize+margin) + 0.5*cellsize;
	var yend = rend*(cellsize+margin) + 0.5*cellsize;

	var vec = createVector(xend,yend);
	vec.sub(xbegin,ybegin);

	var vec2  = createVector(vec.x,vec.y);

	var dist = vec.mag();
	if(lettersPos[begin]%2==0){
	vec.rotate(HALF_PI);
	}
	else{
	vec.rotate(-HALF_PI);
	}
	vec.normalize();
	vec.mult(cellsize*1.0);


	if(lettersPos[begin]%2==0){
	vec2.mult(-0.3);
	}
	else{
	vec2.mult(0.3);
	}



	vec.add(vec2);

	var vecmid = createVector(vec.x,vec.y);
	vecmid.add(new p5.Vector(xbegin,ybegin));

	var vecmid2 = createVector(vec.x,vec.y);
	vecmid2.rotate(PI);
	vecmid2.add(new p5.Vector(xend,yend));


	/*
	var xmid = (xbegin+xend)/2+vec.x;
	var ymid = (ybegin+yend)/2+vec.y;
	*/
	var xmid = vec.x;
	var ymid = vec.y;

	var xmid2;
	var ymid2;

	strokeWeight(0.01*cellsize*1.8*(lettersPos[begin]%3+1));
	push();
	translate(leftMargin,0);
	stroke(0);
//	line(xend,yend, xbegin,ybegin);	
//	bezier(xbegin,ybegin, xmid, ymid, xmid, ymid, xend, yend);
	bezier(xbegin,ybegin, vecmid.x, vecmid.y, vecmid2.x, vecmid2.y, xend, yend);


	pop();


}



